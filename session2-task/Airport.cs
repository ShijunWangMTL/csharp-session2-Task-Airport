﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace session2_task
{
    //- Add getters and setters with verification that throw InvalidDataException on any failure.
    //- Add constructor that takes 5 parameters to initialize the 5 fields.
    //- Add constructor that takes 1 string parameter which is a line in file that it will parse and use to create Airport object.
    //- Add ToString implementation to display data in user-friendly manner.
    //- Add ToDataString implementation to save data to file as a single line semicolon-separated.
    class Airport
    {
        /*Airport(string code, string city, double lat, double lng, int elevM) { ... }
		Airport(string dataLine) { ... }
		string Code; // exactly 3 uppercase letters, use regex
		string City; // 1-50 characters, made up of uppercase and lowercase letters, digits, and .,- characters
		double Latitude, Longitude; // -90 to 90, -180 to 180
		int ElevationMeters; -1000 to 10000
		string ToString() { ... }
		string ToDataString() { ... }*/


        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                Regex rx = new Regex(@"^[A-Z]{3}$");
                if (!rx.IsMatch(value))
                {
                    //Logger?.Invoke(ErrorMessages.CODE_AIRPORT_ERROR_VALIDATION);
                    throw new InvalidDataException(ErrorMessages.CODE_AIRPORT_ERROR_VALIDATION);
                }
                _code = value;
            }
        }

        private string _city;
        public string City
        {
            get { return _city; }
            set
            {
                Regex rx = new Regex(@"^[\s*A-Za-z0-9.-]{1,50}$");
                if (!rx.IsMatch(value))
                {
                   // Logger?.Invoke("City needs to be validated.");
                    throw new InvalidDataException(ErrorMessages.CITY_AIRPORT_ERROR_VALIDATION);
                }
                _city = value;
            }
        }

        private double _latitude;
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if (value < -90 || value > 90)
                {
                  // Logger?.Invoke("Latitude needs to be validated.");
                    throw new InvalidDataException(ErrorMessages.LATITUDE_AIRPORT_ERROR_VALIDATION);
                }
                _latitude = value;
            }
        }

        private double _longitude;
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if (value < -180 || value > 190)
                {
                   // Logger?.Invoke("Longitude needs to be validated.");
                    throw new InvalidDataException(ErrorMessages.LONGITUDE_AIRPORT_ERROR_VALIDATION);
                }
                _longitude = value;
            }
        }

        private int _elevationMeters;
        public int ElevationMeters
        {
            get { return _elevationMeters; }
            set
            {
                if (value < -1000 || value > 10000)
                {
                   // Logger?.Invoke("Elevation meters needs to be validated.");
                    throw new InvalidDataException(ErrorMessages.ELEVATION_METERS_AIRPORT_ERROR_VALIDATION);
                }
                _elevationMeters = value;
            }
        }

        public Airport()
        {
        }

        public Airport(string code, string city, double latitude, double longitude, int em)
        {
            Code = code;
            City = city;
            Latitude = latitude;
            Longitude = longitude;
            ElevationMeters = em;
        }

        public Airport(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                Logger?.Invoke("Line has invalid value.");
                throw new InvalidDataException("Line has invalid value for: \n" + dataLine);
            }

            Code = data[0];
            City = data[1];

            double lat;
            if (!double.TryParse(data[2], out lat))
            {
                Logger?.Invoke("Error: invalid latitude.");
                throw new InvalidDataException("Latitude must be a double number: \n" + dataLine);
            }
            Latitude = lat;

            double lon;
            if (!double.TryParse(data[3], out lon))
            {
                Logger?.Invoke("Error: invalid longitude.");
                throw new InvalidDataException("Longitude must be a double number: \n" + dataLine);
            }
            Longitude = lon;

            int em;
            if (!int.TryParse(data[4], out em))
            {
                Logger?.Invoke("Error: invalid elevation meters.");
                throw new InvalidDataException("ElevationMeters must be an integer: \n" + dataLine);
            }
            ElevationMeters = em;

        }




        public override string ToString()
        {
            return string.Format("{0} in {1} at {2} lat / {3} long {4} elevation", Code, City, Latitude, Longitude, ElevationMeters);
        }
        public string ToDataLine()
        {
            return $"{Code};{City};{Latitude};{Longitude};{ElevationMeters}";
        }


        //static field named Logger of LoggerDelegate
        //delegate type LoggerDelegate that returns void and takes 1 parameter which is the message to be logged

        public delegate void LoggerDelegate(string reason);
        public static LoggerDelegate Logger = null;

        //call Logger for each of the following events in AIrport:
        //- contructor called
        //- exception thrown in setter or constructor
    }
}
