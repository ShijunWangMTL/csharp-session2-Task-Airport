﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace session2_task
{
    static class ErrorMessages
    {
        public const string CODE_AIRPORT_ERROR_VALIDATION = "Code needs to be 3 uppercase letters";
        public const string CITY_AIRPORT_ERROR_VALIDATION = "Airport code must be between 1-50 characters, and made up of uppercase and lowercase letters, digits, and .,- characters.";
        public const string LATITUDE_AIRPORT_ERROR_VALIDATION = "Latitude must be within the range [-90, 90].";
        public const string LONGITUDE_AIRPORT_ERROR_VALIDATION = "Longitude must be within the range [-180, 180].";
        public const string ELEVATION_METERS_AIRPORT_ERROR_VALIDATION = "Elevation meters must be within the range [-1000, 10000].";

    }
}
