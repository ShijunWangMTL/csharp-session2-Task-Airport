﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace session2_task
{

    //Define custom exception InvalidDataException with one constructor that takes string as parameter - the message describing the problem.
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg)
        {

        }
    }

    class Program
    {
        const string DataFileName = @"..\..\airport.txt";

        static List<Airport> AirportList = new List<Airport>();

        static void Main(string[] args)
        {
            // test regex
            //Airport airport1 = new Airport("AC", "Aa 1a.aa", 1.1, 2.2, 11);

            // test datetime ToString
            string dtStr = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            Console.WriteLine(dtStr);

            ///////////////////////////////////////////////////////////////

            ReadDataFromFile();
            Console.WriteLine("Data loaded");

            while (true)
            {
                int choice = GetMenuChoice();
                switch (choice)
                {
                    case 1:
                        // Add Airport
                        AddAirport();
                        break;
                    case 2:
                        // List all airports
                        ListAllAirports();
                        break;
                    case 3:
                        // Find nearest airport by code (preferably using LINQ)
                        FindNearestAirport();
                        break;
                    case 4:
                        // calculate the std deviation
                        stdDeviation();
                        break;
                    case 5:
                        //logging option
                        GetLogOption();
                        break;
                    case 0:
                        // Exit
                        WriteDataToFile();
                        Console.WriteLine("Data saved back to file");
                        return;
                    default:
                        Console.WriteLine("Error: invalid control flow in menu");
                        break;
                }
            }

        }



        //implement a program that repeatedly displays and handles options of the following menu items:

        public static int GetMenuChoice()
        {
            while (true)
            {
                Console.Write(
@"1. Add Airport
2. List all airports
3. Find nearest airport by code 
4. Find airport's elevation standard deviation 
5. Change log delegates
0. Exit
Please enter your choice: ");

                string choiceStr = Console.ReadLine();
                int choice;
                if (!int.TryParse(choiceStr, out choice))
                {
                    Console.WriteLine("Please enter a number between 0 and 5.");
                    continue;   // exit app without continue
                }

                return choice;
            }
        }

        public static void GetLogOption()
        {
            Airport.Logger = null;
            Console.Write(
@"1.Log to file
2. Log to console
3. Log to file and console
Please enter your option: ");

            string optionStr = Console.ReadLine();

            if (optionStr.Equals("1"))
            {
                Airport.Logger = LogToFile;
                Console.WriteLine("Logging to file enabled");
            }
            else if (optionStr.Equals("2"))
            {
                Airport.Logger = LogToConsole;
                Console.WriteLine("Logging to console enabled");
            }
            else if (optionStr.Equals("3"))
            {
                Airport.Logger = LogToFile;
                Airport.Logger += LogToConsole;
                Console.WriteLine("Logging to file enabled \nLogging to console enabled");
            }
            else
            {
                Airport.Logger = null;
                Console.WriteLine("Invalid value entered");
            }

            // **************2nd solution
            /*Console.Write(
@"1. Logging to console
2. Logging to file
Enter your choices, comma-separated, empty for none: ");
            string choiceStr = Console.ReadLine();
            if (choiceStr.Equals("1"))
            {
                Console.WriteLine("Logging to console enabled");
                Airport.Logger += LogToConsole;

            }
            else if (choiceStr.Equals("2"))
            {
                Console.WriteLine("Logging to File enabled");
                Airport.Logger += LogToFile;
            }
            else if (choiceStr.Equals("1,2"))
            {
                Console.WriteLine("Logging to console enabled");
                Console.WriteLine("Logging to File enabled");
                Airport.Logger += LogToConsole;
                Airport.Logger += LogToFile;
            }
            else if (choiceStr.Length == 0)
            {
                Airport.Logger = null;
            }
            else
            {
                Console.WriteLine("Error: You enter a wrong Option");
            }*/

        }


        public static void AddAirport()
        {
            Airport ap = new Airport();
            // Console.WriteLine(ap.Code == null);
            // Console.WriteLine("Please enter the airport code: ");
            while (ap.Code == null)
            {
                try
                {
                    Console.Write("Please enter the airport code: ");
                    ap.Code = Console.ReadLine();
                }
                catch (InvalidDataException exc)
                {
                    Airport.Logger?.Invoke("Error: invalid code.");
                    Console.WriteLine(exc.Message);
                }
            }
            while (ap.City == null)
            {
                try
                {
                    Console.Write("Please enter the city where the airport locates: ");
                    ap.City = Console.ReadLine();
                }
                catch (InvalidDataException exc)
                {
                    Airport.Logger?.Invoke("Error: invalid city.");
                    Console.WriteLine(exc.Message);
                }
            }

            //Console.WriteLine(ap.Latitude); default value = 0
            Console.Write("Please enter the latitude of the airport location: ");
            string latStr = Console.ReadLine();
            double lat = 91;
            while (lat < -90 || lat > 90)
            {                              
                if (!double.TryParse(latStr, out lat))
                {
                    Airport.Logger?.Invoke("Error: invalid latitude value.");
                    Console.WriteLine("Error: latitude must be a valid Double.");
                    Console.Write("Please enter the latitude of the airport location: ");
                    latStr = Console.ReadLine();
                    lat = 91;
                }
                else
                {
                    try
                    {
                        ap.Latitude = lat;
                    }
                    catch (InvalidDataException exc)
                    {
                        Airport.Logger?.Invoke("Error: invalid latitude value.");
                        Console.WriteLine("Error: " + exc.Message);
                        Console.Write("Please enter the latitude of the airport location: ");
                        latStr = Console.ReadLine();
                    }
                }                
            }

            Console.Write("Please enter the longitude of the airport location: ");
            string lonStr = Console.ReadLine();
            double lon = 181;
            while (lon < -180 || lon > 180)
            {
                if (!double.TryParse(lonStr, out lon))
                {
                    Airport.Logger?.Invoke("Error: invalid longitude value.");
                    Console.WriteLine("Error: longitude must be a valid Double.");
                    Console.Write("Please enter the longitude of the airport location: ");
                    lonStr = Console.ReadLine();
                    lon = 181;
                }
                else
                {
                    try
                    {
                        ap.Longitude = lon;
                    }
                    catch (InvalidDataException exc)
                    {
                        Airport.Logger?.Invoke("Error: invalid longitude value.");
                        Console.WriteLine("Error: " + exc.Message);
                        Console.Write("Please enter the longitude of the airport location: ");
                        lonStr = Console.ReadLine();
                    }
                }
            }

            Console.Write("Please enter the elevation meters of the airport location: ");
            string eleStr = Console.ReadLine();
            int ele = 10001;
            while (ele < -1000 || ele > 10000)
            {
                if (!int.TryParse(eleStr, out ele))
                {
                    Airport.Logger?.Invoke("Error: invalid elevationMeters value.");
                    Console.WriteLine("Error: elevationMeters must be a valid integer.");
                    Console.Write("Please enter the elevationMeters of the airport location: ");
                    eleStr = Console.ReadLine();
                    ele = 181;
                }
                else
                {
                    try
                    {
                        ap.ElevationMeters = ele;
                    }
                    catch (InvalidDataException exc)
                    {
                        Airport.Logger?.Invoke("Error: invalid elevationMeters value.");
                        Console.WriteLine("Error: " + exc.Message);
                        Console.Write("Please enter the elevationMeters of the airport location: ");
                        eleStr = Console.ReadLine();
                    }
                }
            }

            try
            {
                AirportList.Add(ap);
                Console.WriteLine("The airport is added.");
            }
            catch (InvalidDataException exc)
            {
                Airport.Logger?.Invoke("Error: adding airport");
                Console.WriteLine(exc.Message);
                return;
            }

            /* Console.Write("Please enter the latitude of the airport location: ");
             string latStr = Console.ReadLine();
             double lat;
             if (!double.TryParse(latStr, out lat))
             {
                 Airport.Logger?.Invoke("Error: latitude must be a valid double number");
                 Console.WriteLine("Error:latitude must be a valid double number");
                 //throw new InvalidDataException("Latitude must be a double number.");
                 return;
             }

             Console.Write("Please enter the longitude of the airport location: ");
             string lonStr = Console.ReadLine();
             double lon;
             if (!double.TryParse(lonStr, out lon))
             {
                 Airport.Logger?.Invoke("Error: longitude must be a valid double number");
                 Console.WriteLine("Error:longitude must be a valid double number");
                 //throw new InvalidDataException("Longitude must be a double number.");
                 return;
             }

             Console.Write("Please enter the elevation meters: ");
             string emStr = Console.ReadLine();
             int em;
             if (!int.TryParse(emStr, out em))
             {
                 Airport.Logger?.Invoke("Error: ElevationMeters must be a valid double number");
                 Console.WriteLine("Error:ElevationMeters must be a valid double number");
                 //throw new InvalidDataException("ElevationMeters must be an integer.");
                 return;
             }
             try
             {
                 AirportList.Add(new Airport(code, city, lat, lon, em));
                 Console.WriteLine("The airport is added.");
                 // AirportList.Add(ap);
             }
             catch (InvalidDataException exc)
             {
                 Airport.Logger?.Invoke("Error: adding airport");
                 Console.WriteLine(exc.Message);
                 return;
             }*/


        }

        private static void ListAllAirports()
        {
            Console.WriteLine(string.Join("\n", AirportList));

            /*foreach (Airport ap in AirportList)
            {
                Console.WriteLine(ap);
            }*/

            bool isEmpty = !AirportList.Any();
            if (isEmpty)
            {
                Console.WriteLine("No airport information.");
            }
        }

        public static void FindNearestAirport()
        {
            //ask user to enter the airport code
            Console.WriteLine("Please enter the airport code: ");
            string code = Console.ReadLine();
            Airport airport = AirportList.Find(airp => airp.Code.Equals(code));
            GeoCoordinate coord = new GeoCoordinate(airport.Latitude, airport.Longitude);

            Airport nearestAp = (from h in AirportList
                                 let geo = new GeoCoordinate { Latitude = h.Latitude, Longitude = h.Longitude }
                                 orderby geo.GetDistanceTo(coord)
                                 select h).Take(2).ToList<Airport>()[1];

            double dist = distance(coord.Latitude, nearestAp.Latitude,
                            coord.Longitude, nearestAp.Longitude);

            Console.WriteLine("Found nearest airport to be {0}/{1} distance is {2}", nearestAp.Code,
                                nearestAp.City, dist);

        }

        public static double toRadians(double angleIn10thofaDegree)
        {
            // angle in 10th of a degree
            return (angleIn10thofaDegree * Math.PI) / 180;
        }

        private static double distance(double lat1, double lat2, double lon1, double lon2)
        {
            // The math module contains a function named toRadians which converts from degrees to radians
            lat1 = toRadians(lat1);
            lat2 = toRadians(lat2);
            lon1 = toRadians(lon1);
            lon2 = toRadians(lon2);

            //haversine formula
            double dlon = lon2 - lon1;
            double dlat = lat2 - lat1;
            double a = Math.Pow(Math.Sin(dlat / 2), 2) +
                       Math.Cos(lat1) * Math.Cos(lat2) *
                       Math.Pow(Math.Sin(dlon / 2), 2);
            double c = 2 * Math.Asin(Math.Sqrt(a));

            // radius of earth in km. use 3956 for miles
            double r = 6371;

            // calculate the result
            return (c * r);

        }


        public static void stdDeviation()
        {
            double sum = 0;
            int count = 0;
            List<int> elevationList = new List<int>();
            foreach (Airport ap in AirportList)
            {
                elevationList.Add(ap.ElevationMeters);
                sum += ap.ElevationMeters;
                count++;
            }
            if (count > 0)
            {
                double avg = sum / count;
                double sumOfSquares = 0;
                foreach (int elevation in elevationList)
                {
                    double squareOfDiff = (elevation - avg) * (elevation - avg);
                    sumOfSquares += squareOfDiff;
                }
                double stdDev = Math.Sqrt(sumOfSquares / elevationList.Count);

                Console.WriteLine("For all airports the standard deviation of their elevation is {0}, stdDev");
            }
            else
            {
                Console.WriteLine("Error: reading the related data");
            }

        }



        //  ****************************read/write data methods
        //Data will be read once when program begins and written back once when program is about to finish.
        public static void ReadDataFromFile()
        {
            if (File.Exists(DataFileName))
            {
                try
                {
                    string[] lineArray = File.ReadAllLines(DataFileName);
                    foreach (string line in lineArray)
                    {
                        try
                        {
                            AirportList.Add(new Airport(line));
                        }
                        catch (InvalidDataException exc)
                        {
                            Console.WriteLine("Error: " + exc.Message);
                        }
                    }
                }
                catch (IOException exc)
                {
                    // Airport.Logger?.Invoke("error in the file " + exc.Message);  // not required
                    Console.WriteLine("Error in the file " + exc.Message);
                }
            }
        }

        public static void WriteDataToFile()
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(DataFileName))
                {
                    foreach (Airport airport in AirportList)
                    {
                        writer.WriteLine(airport.ToDataLine());
                    }
                }

            }
            catch (IOException exc)
            {
                Console.WriteLine("Error writing file: " + exc.Message);
            }
        }


        //  ****************************log methods
        //Initially Logger is null - no logging occurs.
        //Logger methods will append to @"..\..\events.log" file.
        //Logging to file must prefix each line with time stamp in yyyy-mm-dd hh:mm:ss format.
        public static void LogToFile(String msg)
        {
            try
            {
                string dt = DateTime.Now.ToString("yyyy-mm-dd hh:mm:ss");
                msg = dt + " " + msg + "\n";
                File.AppendAllText(@"..\..\events.log", msg);
            }
            catch (IOException exc)
            {
                Console.WriteLine("Error writing file: " + exc.Message);
            }
        }

        public static void LogToConsole(string msg)
        {
            Console.WriteLine(DateTime.Now.ToString("yyyy-mm-dd hh:mm:ss ") + msg);
        }





    }
}
